# Treehouse Emoji

**This repo is unstable. Expect folder structure and vector image changes.**

All are welcome to use and re-purpose our emoji files as they see fit.


## Pride Set

![:pride_agender:](dist/pride/pride-agender.png?raw=true ":pride_agender:")
![:pride_aromantic:](dist/pride/pride-aromantic.png?raw=true ":pride_aromantic:")
![:pride_asexual:](dist/pride/pride-asexual.png?raw=true ":pride_asexual:")
![:pride_black_trans:](dist/pride/pride-black-trans.png?raw=true ":pride_black_trans:")
![:pride_bigender:](dist/pride/pride-bigender.png?raw=true ":pride_bigender:")
![:pride_bisexual:](dist/pride/pride-bisexual.png?raw=true ":pride_bisexual:")
![:pride_demiboy:](dist/pride/pride-demiboy.png?raw=true ":pride_demiboy:")
![:pride_demigirl:](dist/pride/pride-demigirl.png?raw=true ":pride_demigirl:")
![:pride_demisexual:](dist/pride/pride-demisexual.png?raw=true ":pride_demisexual:")
![:pride_gay_male:](dist/pride/pride-gay-male.png?raw=true ":pride_gay_male:")
![:pride_genderfluid:](dist/pride/pride-genderfluid.png?raw=true ":pride_genderfluid:")
![:pride_genderqueer:](dist/pride/pride-genderqueer.png?raw=true ":pride_genderqueer:")
![:pride_intersex:](dist/pride/pride-intersex.png?raw=true ":pride_intersex:")
![:pride_lesbian:](dist/pride/pride-lesbian.png?raw=true ":pride_lesbian:")
![:pride_lesbian2:](dist/pride/pride-lesbian2.png?raw=true ":pride_lesbian2:")
![:pride_nonbinary:](dist/pride/pride-nonbinary.png?raw=true ":pride_nonbinary:")
![:pride_omnisexual:](dist/pride/pride-omnisexual.png?raw=true ":pride_omnisexual:")
![:pride_pansexual:](dist/pride/pride-pansexual.png?raw=true ":pride_pansexual:")
![:pride_polyamy:](dist/pride/pride-polyam.png?raw=true ":pride_polyam:")
![:pride_polyamy2:](dist/pride/pride-polyam2.png?raw=true ":pride_polyam2:")
![:pride_prorgress:](dist/pride/pride-progress.png?raw=true ":pride_progress:")
![:pride_prorgress2:](dist/pride/pride-progress2.png?raw=true ":pride_progress2:")
![:pride_rainbow:](dist/pride/pride-rainbow.png?raw=true ":pride_rainbow:")
![:pride_trans:](dist/pride/pride-trans.png?raw=true ":pride_trans:")
![:pride_transfemme:](dist/pride/pride-transfemme.png?raw=true ":pride_transfemme:")
![:pride_transmasc:](dist/pride/pride-transmasc.png?raw=true ":pride_transmasc:")


## Licenses

All images are published through the CC0 1.0 Universal (CC0 1.0) Public Domain Dedication.

All other files are under The 3-Clause BSD License.


## Attribution

Demiboy and Demigirl flags based on transrants designs (since taken down).

Demisexual Flag based on @AnonMoos@wikimedia.org's SVG (CC0 1.0).

Intersex Flag is based on Morgan Carpenter's [SVG design](https://ihra.org.au/22773/an-intersex-flag/) (CC0 1.0).

New Polyamory Flag courtesy of [PolyAmProud](https://polyamproud.com/flag) (CC0).

Omnisexual flag based on Pastelmemer's design (since taken down).

Old Polyamory Flag design (`pride_polyamory2`) is based on Jim Evan's SVG (CC0 1.0).

Progress Flag is based on @Nikki@wikimedia.org's SVG (CC0 1.0).

Transgender Flag is based on Dlloyd's SVG (Public Domain).
